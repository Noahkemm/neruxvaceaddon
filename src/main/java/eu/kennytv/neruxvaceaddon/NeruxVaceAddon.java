package eu.kennytv.neruxvaceaddon;

import com.google.common.collect.Sets;
import eu.kennytv.neruxvaceaddon.module.*;
import eu.kennytv.neruxvaceaddon.module.base.NVModule;
import eu.kennytv.neruxvaceaddon.setting.LobbySetting;
import eu.kennytv.neruxvaceaddon.util.GameMode;
import net.labymod.api.LabyModAddon;
import net.labymod.ingamegui.ModuleCategory;
import net.labymod.ingamegui.ModuleCategoryRegistry;
import net.labymod.settings.elements.BooleanElement;
import net.labymod.settings.elements.ControlElement;
import net.labymod.settings.elements.SettingsElement;
import net.labymod.utils.Consumer;
import net.labymod.utils.Material;

import java.util.List;
import java.util.Set;

public final class NeruxVaceAddon extends LabyModAddon {
    private static final String PREFIX = "\u00a78\u00bb \u00a7cNeruxVace \u00a78\u00a7l\u258f \u00a77";
    private final ModuleCategory module = new ModuleCategory("NeruxVace", true, new ControlElement.IconData("neruxvace/textures/icon.png"));
    private final Set<NVModule> modules = Sets.newHashSet();
    private GameMode gameMode;
    private boolean showRanking;
    private boolean isNeruxVace;
    private long lastGameStart;

    @Override
    public void onEnable() {
        api.registerServerSupport(this, new NeruxVaceServer(this));
        gameMode = GameMode.NONE;
    }

    @Override
    public void onDisable() {
    }

    @Override
    public void loadConfig() {
        showRanking = !getConfig().has("show-ranking") || getConfig().get("show-ranking").getAsBoolean();
        ModuleCategoryRegistry.loadCategory(module);
        new CookieModule(this);
        new PointModule(this);
        new KillModule(this);
        new DeathModule(this);
        new LossModule(this);
        new WinModule(this);
        new WinAverageModule(this);
        new ElapsedTimeModule(this);
        new IronSpawnModule(this);
        new GoldSpawnModule(this);
        new BrokenBedModule(this);

        new LobbySetting(this);
    }

    @Override
    protected void fillSettings(final List<SettingsElement> list) {
        final BooleanElement element = new BooleanElement("Ranking neben Statanzeigen", new ControlElement.IconData(Material.COMMAND), new Consumer<Boolean>() {
            @Override
            public void accept(final Boolean enabled) {
                showRanking = enabled;
                getConfig().addProperty("show-ranking", showRanking);
                saveConfig();
            }
        }, showRanking);
        list.add(element);
    }

    public String getPrefix() {
        return PREFIX;
    }

    public ModuleCategory getModule() {
        return module;
    }

    public boolean isNeruxVace() {
        return isNeruxVace;
    }

    public void setNeruxVace(final boolean neruxvace) {
        this.isNeruxVace = neruxvace;
    }

    public GameMode getGameMode() {
        return gameMode;
    }

    public void setGameMode(final GameMode gameMode) {
        this.gameMode = gameMode;
    }

    public Set<NVModule> getModules() {
        return modules;
    }

    public long getLastGameStart() {
        return lastGameStart;
    }

    public void setLastGameStart(final long lastGameStart) {
        this.lastGameStart = lastGameStart;
    }

    public boolean showRanking() {
        return showRanking;
    }
}
