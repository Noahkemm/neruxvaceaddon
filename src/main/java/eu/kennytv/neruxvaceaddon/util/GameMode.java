package eu.kennytv.neruxvaceaddon.util;

import com.google.common.collect.Sets;

import java.util.Set;

public enum GameMode {

    GAME_MODE("BedWars", "StickDuell", "WoolBattle", "ClanWars-Training", "SkyWars"),
    STATIC_MODE("Build", "SkyPvP", "Fantasy"),
    LOBBY("lobby"),
    NONE;

    private final Set<String> servers;
    private String current;

    GameMode(final String... servers) {
        this.servers = Sets.newHashSet(servers);
    }

    public Set<String> getServers() {
        return servers;
    }

    public String getCurrent() {
        return current;
    }

    public void setCurrent(final String current) {
        this.current = current;
    }
}
