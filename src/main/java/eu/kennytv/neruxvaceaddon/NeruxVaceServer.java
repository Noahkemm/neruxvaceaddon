package eu.kennytv.neruxvaceaddon;

import eu.kennytv.neruxvaceaddon.module.base.NVModule;
import eu.kennytv.neruxvaceaddon.util.GameMode;
import net.labymod.api.events.TabListEvent;
import net.labymod.servermanager.ChatDisplayAction;
import net.labymod.servermanager.Server;
import net.labymod.settings.elements.SettingsElement;
import net.labymod.utils.Consumer;
import net.minecraft.client.multiplayer.ServerData;
import net.minecraft.network.PacketBuffer;

import java.util.List;
import java.util.concurrent.TimeUnit;

public final class NeruxVaceServer extends Server {
    private final NeruxVaceAddon plugin;

    NeruxVaceServer(final NeruxVaceAddon plugin) {
        super("NeruxVace.net", "NeruxVace.net", "TheVace.net", "Nerux.net", "NeruxVase.net");
        this.plugin = plugin;
        plugin.getApi().getEventManager().registerOnQuit(new Consumer<net.labymod.utils.ServerData>() {
            @Override
            public void accept(final net.labymod.utils.ServerData serverData) {
                if (!plugin.isNeruxVace()) return;
                plugin.setNeruxVace(false);
                for (final NVModule module : plugin.getModules())
                    module.resetStat();
            }
        });
    }

    @Override
    public void onJoin(final ServerData data) {
        plugin.setNeruxVace(true);
        plugin.setGameMode(GameMode.LOBBY);
    }

    @Override
    public ChatDisplayAction handleChatMessage(final String s, final String s1) {
        if (!s.startsWith("»") || plugin.getGameMode() != GameMode.GAME_MODE) return null;
        if (s.endsWith("Die Runde beginnt!"))
            plugin.setLastGameStart(TimeUnit.MILLISECONDS.toSeconds(System.currentTimeMillis()));
        else if (s.endsWith("gewonnen!"))
            plugin.setLastGameStart(0);
        return null;
    }

    @Override
    public void handlePluginMessage(final String s, final PacketBuffer packetBuffer) {
    }

    @Override
    public void handleTabInfoMessage(final TabListEvent.Type type, final String s, final String s1) {
        if (type != TabListEvent.Type.HEADER) return;

        boolean found = false;
        for (final GameMode gameMode : GameMode.values()) {
            for (final String server : gameMode.getServers()) {
                if (s.contains(server)) {
                    plugin.setGameMode(gameMode);
                    gameMode.setCurrent(server);
                    plugin.setLastGameStart(0);
                    found = true;
                    break;
                }
            }
            if (found) break;
        }


        if (found) {
            for (final NVModule module : plugin.getModules())
                module.resetStat();
        } else
            plugin.setGameMode(GameMode.NONE);


        for (final NVModule module : plugin.getModules()) {
            if (module.getGameMode() != plugin.getGameMode()) continue;
            module.sendStatRequest();
        }
    }

    @Override
    public void fillSubSettings(final List<SettingsElement> list) {
    }
}
