package eu.kennytv.neruxvaceaddon.setting;

import com.google.gson.JsonObject;
import eu.kennytv.neruxvaceaddon.NeruxVaceAddon;
import eu.kennytv.neruxvaceaddon.util.GameMode;
import net.labymod.gui.elements.DropDownMenu;
import net.labymod.settings.elements.DropDownElement;
import net.labymod.settings.elements.SettingsElement;
import net.labymod.utils.Consumer;

import java.util.List;

public final class LobbySetting {
    private final NeruxVaceAddon plugin;
    private PlayerVisibility playerVisibility;

    public LobbySetting(final NeruxVaceAddon plugin) {
        this.plugin = plugin;
        playerVisibility = plugin.getConfig().has("player-visibility") ? PlayerVisibility.valueOf(plugin.getConfig().get("player-visibility").getAsString()) : PlayerVisibility.ALL;
        loadSettings(plugin.getSubSettings());
    }

    private void loadSettings(final List<SettingsElement> list) {
        final DropDownMenu<PlayerVisibility> menu = new DropDownMenu<PlayerVisibility>("Lobby Spieler Sichtbarkeit", 0, 0, 0, 0).fill(PlayerVisibility.values());
        final DropDownElement<PlayerVisibility> droppedMenu = new DropDownElement<PlayerVisibility>("Lobby Spieler Sichtbarkeit", menu);
        menu.setSelected(playerVisibility);
        droppedMenu.setChangeListener(new Consumer<PlayerVisibility>() {
            @Override
            public void accept(final PlayerVisibility visibility) {
                if (plugin.getGameMode() != GameMode.LOBBY) {
                    menu.setSelected(playerVisibility);
                    plugin.getApi().displayMessageInChat(plugin.getPrefix() + "\u00a7cDu musst hierf\u00fcr auf einem Lobbyserver auf dem Netzwerk sein!");
                    return;
                }

                playerVisibility = visibility;
                plugin.getConfig().addProperty("player-visibility", visibility.name());
                plugin.saveConfig();

                final JsonObject optionsObject = new JsonObject();
                optionsObject.addProperty("visibility", visibility.getId());
                plugin.getApi().sendJsonMessageToServer("NV#playerVisibility", optionsObject);
            }
        });

        list.add(droppedMenu);
    }

    private enum PlayerVisibility {

        ALL(0),
        YOUTUBER(1),
        TEAM(2),
        NONE(3);

        private final int id;

        PlayerVisibility(final int id) {
            this.id = id;
        }

        public int getId() {
            return id;
        }
    }
}
