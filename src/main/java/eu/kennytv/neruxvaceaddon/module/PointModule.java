package eu.kennytv.neruxvaceaddon.module;

import eu.kennytv.neruxvaceaddon.util.GameMode;
import eu.kennytv.neruxvaceaddon.NeruxVaceAddon;
import eu.kennytv.neruxvaceaddon.module.base.NVModule;
import net.labymod.settings.elements.ControlElement;
import net.labymod.utils.Material;

public final class PointModule extends NVModule {

    public PointModule(final NeruxVaceAddon plugin) {
        super(plugin);
    }

    @Override
    public String getDisplayName() {
        return "Punkte";
    }

    @Override
    public ControlElement.IconData getIconData() {
        return new ControlElement.IconData(Material.DIAMOND);
    }

    @Override
    public String getDescription() {
        return "Anzahl an Punkten";
    }

    @Override
    protected String getStatName() {
        return "points";
    }

    @Override
    public GameMode getGameMode() {
        return GameMode.LOBBY;
    }
}
