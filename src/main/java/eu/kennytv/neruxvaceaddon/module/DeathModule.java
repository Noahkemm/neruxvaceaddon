package eu.kennytv.neruxvaceaddon.module;

import eu.kennytv.neruxvaceaddon.NeruxVaceAddon;
import eu.kennytv.neruxvaceaddon.module.base.NVModule;
import eu.kennytv.neruxvaceaddon.util.GameMode;
import net.labymod.settings.elements.ControlElement;
import net.labymod.utils.Material;

public final class DeathModule extends NVModule {

    public DeathModule(final NeruxVaceAddon plugin) {
        super(plugin);
    }

    @Override
    public String getDisplayName() {
        return "Tode";
    }

    @Override
    public ControlElement.IconData getIconData() {
        return new ControlElement.IconData(Material.WOOD_SWORD);
    }

    @Override
    public String getDescription() {
        return "Anzahl an Toden";
    }

    @Override
    protected String getStatName() {
        return plugin.getGameMode() == GameMode.STATIC_MODE ? "deaths" : "DEATHS";
    }

    @Override
    public GameMode getGameMode() {
        return GameMode.STATIC_MODE;
    }
}
