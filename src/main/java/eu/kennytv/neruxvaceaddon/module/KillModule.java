package eu.kennytv.neruxvaceaddon.module;

import eu.kennytv.neruxvaceaddon.NeruxVaceAddon;
import eu.kennytv.neruxvaceaddon.module.base.NVModule;
import eu.kennytv.neruxvaceaddon.util.GameMode;
import net.labymod.settings.elements.ControlElement;
import net.labymod.utils.Material;

public final class KillModule extends NVModule {

    public KillModule(final NeruxVaceAddon plugin) {
        super(plugin);
    }

    @Override
    public String getDisplayName() {
        return "Kills";
    }

    @Override
    public ControlElement.IconData getIconData() {
        return new ControlElement.IconData(Material.DIAMOND_SWORD);
    }

    @Override
    public String getDescription() {
        return "Anzahl an getöteten Spielern";
    }

    @Override
    protected String getStatName() {
        return plugin.getGameMode() == GameMode.STATIC_MODE ? "kills" : "KILLS";
    }

    @Override
    public GameMode getGameMode() {
        return GameMode.STATIC_MODE;
    }
}
