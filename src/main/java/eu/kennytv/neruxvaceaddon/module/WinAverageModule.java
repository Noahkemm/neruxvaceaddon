package eu.kennytv.neruxvaceaddon.module;

import eu.kennytv.neruxvaceaddon.NeruxVaceAddon;
import eu.kennytv.neruxvaceaddon.module.base.NVModule;
import eu.kennytv.neruxvaceaddon.util.GameMode;
import net.labymod.settings.elements.ControlElement;
import net.labymod.utils.Material;
import net.labymod.utils.ModUtils;

public final class WinAverageModule extends NVModule {

    public WinAverageModule(final NeruxVaceAddon plugin) {
        super(plugin);
    }

    @Override
    public String getDisplayName() {
        return "Sieg%";
    }

    @Override
    public ControlElement.IconData getIconData() {
        return new ControlElement.IconData(Material.EMERALD);
    }

    @Override
    public String getDescription() {
        return "Siegeswahrscheinlichkeit";
    }

    @Override
    protected String getStatName() {
        return "WIN_AVG";
    }

    @Override
    public GameMode getGameMode() {
        return GameMode.GAME_MODE;
    }
}
