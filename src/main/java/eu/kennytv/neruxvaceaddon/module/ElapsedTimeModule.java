package eu.kennytv.neruxvaceaddon.module;

import eu.kennytv.neruxvaceaddon.NeruxVaceAddon;
import eu.kennytv.neruxvaceaddon.module.base.NVModule;
import eu.kennytv.neruxvaceaddon.util.GameMode;
import net.labymod.settings.elements.ControlElement;
import net.labymod.utils.Material;
import net.labymod.utils.ModUtils;

import java.util.concurrent.TimeUnit;

public final class ElapsedTimeModule extends NVModule {

    public ElapsedTimeModule(final NeruxVaceAddon plugin) {
        super(plugin);
    }

    @Override
    public String getDisplayName() {
        return "Rundenzeit";
    }

    @Override
    public String getDisplayValue() {
        return ModUtils.parseTimer((int) ((TimeUnit.MILLISECONDS.toSeconds(System.currentTimeMillis()) - plugin.getLastGameStart())));
    }

    @Override
    public ControlElement.IconData getIconData() {
        return new ControlElement.IconData(Material.WATCH);
    }

    @Override
    public String getDescription() {
        return "Bisher abgelaufene Zeit einer Runde";
    }

    @Override
    public boolean isShown() {
        return getGameMode() == plugin.getGameMode() && plugin.getLastGameStart() != 0;
    }

    @Override
    protected String getStatName() {
        return null;
    }

    @Override
    public GameMode getGameMode() {
        return GameMode.GAME_MODE;
    }

    @Override
    public void sendStatRequest() {
    }
}
