package eu.kennytv.neruxvaceaddon.module;

import eu.kennytv.neruxvaceaddon.NeruxVaceAddon;
import eu.kennytv.neruxvaceaddon.module.base.NVItemModule;
import eu.kennytv.neruxvaceaddon.util.GameMode;
import net.labymod.settings.elements.ControlElement;
import net.labymod.utils.Material;
import net.minecraft.item.Item;
import net.minecraft.item.ItemStack;

import java.util.concurrent.TimeUnit;

public final class IronSpawnModule extends NVItemModule {

    public IronSpawnModule(final NeruxVaceAddon plugin) {
        super(plugin);
    }

    @Override
    public String getControlName() {
        return "BedWars Iron Timer";
    }

    @Override
    public ItemStack getItem() {
        final int elapsedTime = (int) ((TimeUnit.MILLISECONDS.toSeconds(System.currentTimeMillis()) - plugin.getLastGameStart()));
        return new ItemStack(Item.getItemById(265), 15 - elapsedTime % 15);
    }

    @Override
    public ControlElement.IconData getIconData() {
        return new ControlElement.IconData(Material.IRON_INGOT);
    }

    @Override
    public String getDescription() {
        return "BedWars Ironspawn Timer";
    }

    @Override
    public boolean isShown() {
        return getGameMode() == plugin.getGameMode() && plugin.getGameMode().getCurrent().equals("BedWars") && plugin.getLastGameStart() != 0;
    }

    @Override
    protected String getStatName() {
        return null;
    }

    @Override
    public GameMode getGameMode() {
        return GameMode.GAME_MODE;
    }
}
