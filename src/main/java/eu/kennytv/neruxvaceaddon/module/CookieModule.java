package eu.kennytv.neruxvaceaddon.module;

import eu.kennytv.neruxvaceaddon.util.GameMode;
import eu.kennytv.neruxvaceaddon.NeruxVaceAddon;
import eu.kennytv.neruxvaceaddon.module.base.NVModule;
import net.labymod.settings.elements.ControlElement;
import net.labymod.utils.Material;

public final class CookieModule extends NVModule {

    public CookieModule(final NeruxVaceAddon plugin) {
        super(plugin);
    }

    @Override
    public String getDisplayName() {
        return "Cookies";
    }

    @Override
    public ControlElement.IconData getIconData() {
        return new ControlElement.IconData(Material.COOKIE);
    }

    @Override
    public String getDescription() {
        return "Anzahl an Cookies";
    }

    @Override
    protected String getStatName() {
        return "cookies";
    }

    @Override
    public GameMode getGameMode() {
        return GameMode.LOBBY;
    }
}
