package eu.kennytv.neruxvaceaddon.module;

import eu.kennytv.neruxvaceaddon.NeruxVaceAddon;
import eu.kennytv.neruxvaceaddon.module.base.NVModule;
import eu.kennytv.neruxvaceaddon.util.GameMode;
import net.labymod.settings.elements.ControlElement;
import net.labymod.utils.Material;

public final class LossModule extends NVModule {

    public LossModule(final NeruxVaceAddon plugin) {
        super(plugin);
    }

    @Override
    public String getDisplayName() {
        return "Niederlagen";
    }

    @Override
    public ControlElement.IconData getIconData() {
        return new ControlElement.IconData(Material.COAL_BLOCK);
    }

    @Override
    public String getDescription() {
        return "Anzahl an verlorenen Spielen";
    }

    @Override
    protected String getStatName() {
        return "LOSES";
    }

    @Override
    public GameMode getGameMode() {
        return GameMode.GAME_MODE;
    }
}
