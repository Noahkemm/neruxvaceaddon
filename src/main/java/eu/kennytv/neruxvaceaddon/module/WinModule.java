package eu.kennytv.neruxvaceaddon.module;

import eu.kennytv.neruxvaceaddon.NeruxVaceAddon;
import eu.kennytv.neruxvaceaddon.module.base.NVModule;
import eu.kennytv.neruxvaceaddon.util.GameMode;
import net.labymod.settings.elements.ControlElement;
import net.labymod.utils.Material;

public final class WinModule extends NVModule {

    public WinModule(final NeruxVaceAddon plugin) {
        super(plugin);
    }

    @Override
    public String getDisplayName() {
        return "Siege";
    }

    @Override
    public ControlElement.IconData getIconData() {
        return new ControlElement.IconData(Material.EMERALD_BLOCK);
    }

    @Override
    public String getDescription() {
        return "Anzahl an gewonnenen Spielen";
    }

    @Override
    protected String getStatName() {
        return "WINS";
    }

    @Override
    public GameMode getGameMode() {
        return GameMode.GAME_MODE;
    }
}
