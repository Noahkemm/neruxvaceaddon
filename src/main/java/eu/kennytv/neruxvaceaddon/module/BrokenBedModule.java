package eu.kennytv.neruxvaceaddon.module;

import eu.kennytv.neruxvaceaddon.NeruxVaceAddon;
import eu.kennytv.neruxvaceaddon.module.base.NVModule;
import eu.kennytv.neruxvaceaddon.util.GameMode;
import net.labymod.settings.elements.ControlElement;
import net.labymod.utils.Material;

public final class BrokenBedModule extends NVModule {

    public BrokenBedModule(final NeruxVaceAddon plugin) {
        super(plugin);
    }

    @Override
    public String getDisplayName() {
        return "Abgebaute Betten";
    }

    @Override
    public ControlElement.IconData getIconData() {
        return new ControlElement.IconData(Material.BED);
    }

    @Override
    public String getDescription() {
        return "Anzahl abgebauter Betten in BedWars";
    }

    @Override
    public boolean isShown() {
        return getGameMode() == plugin.getGameMode() && plugin.getGameMode().getCurrent().equals("BedWars");
    }

    @Override
    protected String getStatName() {
        return "BEDS_BROKEN";
    }

    @Override
    public GameMode getGameMode() {
        return GameMode.GAME_MODE;
    }
}
