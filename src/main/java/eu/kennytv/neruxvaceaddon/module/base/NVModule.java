package eu.kennytv.neruxvaceaddon.module.base;

import com.google.gson.JsonElement;
import com.google.gson.JsonObject;
import eu.kennytv.neruxvaceaddon.NeruxVaceAddon;
import eu.kennytv.neruxvaceaddon.util.GameMode;
import net.labymod.api.events.ServerMessageEvent;
import net.labymod.ingamegui.ModuleCategory;
import net.labymod.ingamegui.moduletypes.SimpleModule;

public abstract class NVModule extends SimpleModule {
    protected final NeruxVaceAddon plugin;
    private String stat;

    protected NVModule(final NeruxVaceAddon plugin) {
        this.plugin = plugin;
        plugin.getApi().registerModule(this);
        plugin.getModules().add(this);
        plugin.getApi().getEventManager().register(new ServerMessageEvent() {
            @Override
            public void onServerMessage(final String s, final JsonElement element) {
                if (!plugin.isNeruxVace() || !s.equals("NV#stat" + getStatName())) return;
                final JsonObject object = element.getAsJsonObject();
                stat = object.has("stat") ? object.get("stat").getAsString() : null;
                if (object.has("pos"))
                    stat += "\u00a77(\u00a7ePlatz \u00a7e\u00a7l" + object.get("pos").getAsString() + " \u00a77)";
            }
        });
    }

    @Override
    public String getDisplayValue() {
        return stat;
    }

    @Override
    public ModuleCategory getCategory() {
        return plugin.getModule();
    }

    @Override
    public String getDefaultValue() {
        return "?";
    }

    @Override
    public String getSettingName() {
        return getDescription();
    }

    @Override
    public boolean isShown() {
        return getGameMode() == GameMode.LOBBY || getGameMode() == plugin.getGameMode() || plugin.getGameMode() == GameMode.STATIC_MODE && getGameMode() == GameMode.GAME_MODE;
    }

    @Override
    public int getSortingId() {
        return 0;
    }

    @Override
    public void loadSettings() {
    }

    public void resetStat() {
        stat = null;
    }

    public abstract GameMode getGameMode();

    protected abstract String getStatName();

    public void sendStatRequest() {
        final JsonObject jsonObject = new JsonObject();
        jsonObject.addProperty("stat", getStatName());
        if (plugin.showRanking())
            jsonObject.addProperty("pos", "j");
        plugin.getApi().sendJsonMessageToServer("NV#statRequest", jsonObject);
    }
}
