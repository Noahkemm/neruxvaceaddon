package eu.kennytv.neruxvaceaddon.module.base;

import eu.kennytv.neruxvaceaddon.NeruxVaceAddon;
import eu.kennytv.neruxvaceaddon.util.GameMode;
import net.labymod.ingamegui.ModuleCategory;
import net.labymod.ingamegui.moduletypes.ItemModule;
import net.minecraft.util.ResourceLocation;

public abstract class NVItemModule extends ItemModule {
    protected final NeruxVaceAddon plugin;

    protected NVItemModule(final NeruxVaceAddon plugin) {
        this.plugin = plugin;
        plugin.getApi().registerModule(this);
    }

    @Override
    public boolean drawDurability() {
        return false;
    }

    @Override
    public ResourceLocation getPlaceholderTexture() {
        return null;
    }

    @Override
    public ModuleCategory getCategory() {
        return plugin.getModule();
    }

    @Override
    public String getSettingName() {
        return getDescription();
    }

    @Override
    public boolean isShown() {
        return getGameMode() == GameMode.LOBBY || getGameMode() == plugin.getGameMode() || plugin.getGameMode() == GameMode.STATIC_MODE && getGameMode() == GameMode.GAME_MODE;
    }

    @Override
    public int getSortingId() {
        return 0;
    }

    @Override
    public void loadSettings() {
    }

    public abstract GameMode getGameMode();

    protected abstract String getStatName();
}
